// sha160 hash
// Copyright 2017-2020 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// sha1 hash
#pragma once
#include <cstdint>

class Sha1
{
public:
	static constexpr int blocksize{64};
	static constexpr int hashsize{20};
public:
	Sha1() {}

	// hash of data with padding
	void hash(const uint8_t *data, int n) {begin(); add(data, n); end();}

	// hash of data in pieces
	void begin();
	void add(const uint8_t *data, int n);
	void end();

	// return hash
	uint8_t operator[](int i) const {return reinterpret_cast<const uint8_t *>(hh)[i];}
	int size() const {return hashsize;}
public:
	// hash of a single block without padding
	void block();
private:
	uint32_t hh[hashsize/sizeof(uint32_t)];

	// working buffer
	int64_t length{};
	uint32_t w[blocksize/sizeof(uint32_t)];
	uint8_t * const w_begin{reinterpret_cast<uint8_t *>(w)}, * const w_end{w_begin + blocksize};
	uint8_t * w_current{w_begin};
};
