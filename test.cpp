// Copyright 2017 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

// Cryptography library
// These classes and functions are intended for experimentation with
// cryptographic functions. They have not been designed to be secure from
// side channel attacks.

#include "aes.h"
#include "sha1.h"
#include "sha2.h"
#include "hmac.h"
#include "base64.h"
#include "crc.h"
#include <iostream>
#include <iomanip>
#include <chrono>

void display(const uint8_t *data, int n, bool ciphertext)
{
	std::cout << std::hex;
	std::cout.fill('0');
	if (ciphertext)
		std::cout << std::uppercase;
	else
		std::cout << std::nouppercase;
	for (int i = 0; i < n; ++i)
	{
		std::cout << std::setw(2) << (unsigned int){data[i]};
		if (i%4 == 3)
			std::cout << ' ';
	}
	std::cout << std::endl;
}

void test()
{
	using std::cout, std::endl;
	using std::hex, std::dec;

	try
	{
		// test AES128
		{
			std::cout << "* AES128" << std::endl;
			uint8_t key[16] =
			{
				0x2b,0x7e,0x15,0x16,0x28,0xae,0xd2,0xa6,0xab,0xf7,0x15,0x88,0x09,0xcf,0x4f,0x3c
			};
			uint8_t plaintext[64]=
			{
				0x6b,0xc1,0xbe,0xe2,0x2e,0x40,0x9f,0x96,0xe9,0x3d,0x7e,0x11,0x73,0x93,0x17,0x2a,
				0xae,0x2d,0x8a,0x57,0x1e,0x03,0xac,0x9c,0x9e,0xb7,0x6f,0xac,0x45,0xaf,0x8e,0x51,
				0x30,0xc8,0x1c,0x46,0xa3,0x5c,0xe4,0x11,0xe5,0xfb,0xc1,0x19,0x1a,0x0a,0x52,0xef,
				0xf6,0x9f,0x24,0x45,0xdf,0x4f,0x9b,0x17,0xad,0x2b,0x41,0x7b,0xe6,0x6c,0x37,0x10
			};
			display(plaintext, sizeof(plaintext), false);
			Aes128 aes128(key);
			aes128.encrypt(plaintext);
			aes128.encrypt(plaintext+16);
			aes128.encrypt(plaintext+32);
			aes128.encrypt(plaintext+48);
			display(plaintext, sizeof(plaintext), true);
			const int repeats{100000};
			auto t0 = std::chrono::high_resolution_clock::now();
			for (int i = 0; i < repeats; ++i)
				aes128.encrypt(plaintext);
			auto t1 = std::chrono::high_resolution_clock::now();
			std::cerr << "aes " << std::chrono::duration<double>(t1-t0).count()*1000000/repeats << "μs" << std::endl;
		}

		// test SHA1
		{
			std::cout << "* SHA1" << std::endl;
			Sha1 sha1;
			uint8_t buffer[64] = "abc";
			sha1.hash(buffer, 3);
			uint8_t h[20];
			for (int i = 0; i < sha1.size(); ++i)
				h[i] = sha1[i];
			display(h, sizeof(h), true);
			const int repeats{1000000};
			auto t0 = std::chrono::high_resolution_clock::now();
			for (int i = 0; i < repeats; ++i)
				sha1.hash(buffer, 3);
			auto t1 = std::chrono::high_resolution_clock::now();
			std::cerr << "sha1 " << std::chrono::duration<double>(t1-t0).count()*1000000/repeats << "μs" << std::endl;
		}

		// test SHA256
		{
			std::cout << "* SHA256" << std::endl;
			Sha2 sha2;
			uint8_t buffer[64] = "abc";
			sha2.hash(buffer, 3);
			uint8_t h[32];
			for (int i = 0; i < sha2.size(); ++i)
				h[i] = sha2[i];
			display(h, sizeof(h), true);
			const int repeats{1000000};
			auto t0 = std::chrono::high_resolution_clock::now();
			for (int i = 0; i < repeats; ++i)
				sha2.hash(buffer, 3);
			auto t1 = std::chrono::high_resolution_clock::now();
			std::cerr << "sha256 " << std::chrono::duration<double>(t1-t0).count()*1000000/repeats << "μs" << std::endl;
		}

		// test HMAC
		{
			std::cout << "* HMAC" << std::endl;
			Hmac<Sha2> hmac;
			auto h = hmac("Jefe", "what do ya want for nothing?");
			display(reinterpret_cast<const uint8_t *>(h.c_str()), h.size(), true);
		}

		// test base64
		{
			std::cout << "* base64" << std::endl;
			std::cout << Base64encode{}("foob") << std::endl;
			std::cout << Base64encode{}("fooba") << std::endl;
			std::cout << Base64encode{}("foobar") << std::endl;
			std::cout << Base64decode{}(Base64encode{}("foob")) << std::endl;
			std::cout << Base64decode{}(Base64encode{}("fooba")) << std::endl;
			std::cout << Base64decode{}(Base64encode{}("foobar")) << std::endl;
		}

		// test CRC
		{
			// test CRC
			cout << "*** CRC ***" << endl;
			const char *test0 = "CatMouse987654321";
			//math::Crc16_xmodem::pre_initialize();
			math::Crc16_xmodem t0;
			t0 += (const uint8_t *) test0;
			cout << "CRC=" << hex << t0 << endl;
			math::Crc16_X25 t1;
			t1 += (const uint8_t *) test0;
			cout << "CRC=" << hex << t1 << endl;
			math::Crc32_posix t2;
			t2 += (const uint8_t *) test0;
			cout << "CRC=" << hex << t2 << endl;
			math::Crc64_ECMA182 t3;
			t3 += (const uint8_t *) test0;
			cout << "CRC=" << hex << t3 << endl;
			math::Crc64_ISO t4;
			t4 += (const uint8_t *) test0;
			cout << "CRC=" << hex << t4 << endl;
			math::Crc24_Q t7;
			t7 += (const uint8_t *) test0;
			cout << "CRC=" << hex << t7 << endl;
			math::CrcX16_xmodem t5;
			for (auto test = test0; test < test0+17-3; test +=4)
				t5 += * (const uint32_t *) test;
			t5 += (const uint8_t *) (test0+16);
			cout << "CRC=" << hex << t5 << endl;
			math::CrcX32_posix t6;
			for (auto test = test0; test < test0+17-3; test +=4)
				t6 += * (const uint32_t *) test;
			t6 += (const uint8_t *) (test0+16);
			cout << "CRC=" << hex << t6 << endl;
			math::CrcX24_Q t8;
			t8 += (const uint8_t *) test0;
			cout << "CRC=" << hex << t8 << endl;
		}
	}
	catch (std::exception &error)
	{
		std::cerr << "exception " << error.what() << std::endl;
	}
}

// main
int main(int argc, char **argv)
{
	// flags
	int iArg = 1;
	char *arg = argv[1];
	while (iArg < argc && (arg[0] == '-' || arg[0] == '/'))
	{
		char c = tolower(arg[1]);
		switch (c)
		{
		case '?': case 'h':
		default:
			std::cerr << "Usage(V1.0): " << argv[0] << " [-options] filename" << std::endl;
			std::cerr << "   -h     : help" << std::endl;
			return 1;
		}
		arg = argv[++iArg];
	}

	// files
	while (iArg < argc)
	{
		arg = argv[++iArg];
	}
	if (iArg < argc)
	{
		std::cerr <<"Error: too many arguments" << std::endl;
		return 1;
	}

	// test function
	test();
}
