// HMAC
// Copyright 2020 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#pragma once
#include <cassert>
#include <string>
#include <iostream>
#include <iomanip>

template<typename Hash> class Hmac
{
public:
	std::string operator()(std::string key, std::string message)
	{
		uint8_t buffer[Hash::blocksize+1000];
		uint8_t hmessage[Hash::hashsize];
		assert(key.size() < Hash::blocksize); // does not handle long keys

		// pad key and xor with 0x36
		int ksize = key.size();
		for (int i = 0; i < ksize; ++i)
			buffer[i] = key[i] ^ 0x36;
		for (int i = ksize; i < Hash::blocksize; ++i)
			buffer[i] = 0x36;

		// inner hash of key and message
		Hash hash;
		hash.begin();
		hash.add(buffer, Hash::blocksize);
		hash.add(reinterpret_cast<const uint8_t *>(message.c_str()), message.size());
		hash.end();
		for (int i = 0; i < Hash::hashsize; ++i)
			hmessage[i] = hash[i];

		// outer hash of key and inner hash
		for (int i = 0; i < Hash::blocksize; ++i)
			buffer[i] ^= 0x36^0x5C;
		hash.begin();
		hash.add(buffer, Hash::blocksize);
		hash.add(hmessage, Hash::hashsize);
		hash.end();

		// result
		std::string result;
		for (int i = 0; i < hash.size(); ++i)
			result += char(hash[i]);
		return result;
	}
};
