# definitions
LIBRARY = libcrypt
SRC = aes sha1 sha2 base64 crc
APP = test

# definitions
CPPFLAGS = -O2 @gcc.conf -MMD -MP -DNDEBUG
LDFLAGS = -O2

# all
all: test

# executables
$(APP): %: %.o $(LIBRARY).a
	$(CXX) $(LDFLAGS) $^ -o $@

# library functions
$(LIBRARY).a: $(SRC:%=%.o)
	rm -f $@ && ar qc $@ $(SRC:%=%.o)

clean:
	rm -f $(LIBRARY).a *.o

# library
test: test.o $(LIBRARY).a
	./test >/tmp/$$PPID.out && diff -s test.out /tmp/$$PPID.out

#dependencies
-include $(SRC:%=%.d) $(APP:%=%.d)
