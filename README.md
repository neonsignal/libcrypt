collection of cryptography routines including aes128, sha160, sha256, hmac

Copyright 2017-2020 Glenn McIntosh

licensed under the GNU General Public License version 3, see [LICENSE.md](LICENSE.md)
