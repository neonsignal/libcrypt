// SHA160 hash
// Copyright 2017-2020 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

// include files
#include "sha1.h"
#include <algorithm>

// register rotation
template<typename T> T rl(T x, const int i) {return x<<i | x>>(sizeof(T)*8-i);}
template<typename T, int n> constexpr int size(T(&)[n]){return n;}

void Sha1::begin()
{
	length = 0;
	hh[0] = 0x67452301; hh[1] = 0xEFCDAB89; hh[2] = 0x98BADCFE; hh[3] = 0x10325476; hh[4] = 0xC3D2E1F0;
	w_current = w_begin;
}

void Sha1::add(const uint8_t *data, int n)
{
	// hash message blocks
	const uint8_t *datan = data+n, *data1;
	while ((data1 = data+(w_end-w_current)) <= datan)
	{
		std::copy(data, data1, w_current);
		data = data1;
		w_current = w_begin;
		block();
	}

	// save remainder
	std::copy(data, datan, w_current);
	w_current += datan-data;
	length += n;
}

void Sha1::end()
{
	// terminate and finalize hash
	*w_current++ = 0x80;
	if (w_current > w_end-sizeof(uint64_t))
	{
		std::fill(w_current, w_end, 0x00);
		block();
		w_current = w_begin;
	}
	std::fill(w_current, w_end-sizeof(uint64_t), 0x00);
	w[::size(w)-2] = __builtin_bswap32(length*8 >> 32);
	w[::size(w)-1] = __builtin_bswap32(length*8);
	block();

	// back to little endian
	for (int i = 0; i < ::size(hh); ++i)
		hh[i] = __builtin_bswap32(hh[i]);
}


// hash
void Sha1::block()
{
	// change to big endian
	for (int j = 0; j < ::size(w); ++j)
		w[j] = __builtin_bswap32(w[j]);

	// initialize hash
	uint32_t a = hh[0], b = hh[1], c = hh[2], d = hh[3], e = hh[4];

	// round keys
	const uint32_t k[] =
	{
		0x5A827999, 0x6ED9EBA1, 0x8F1BBCDC, 0xCA62C1D6,
	};

	// rounds
	#if !defined(__OPTIMIZE__) || defined(__OPTIMEYE_SIZE__)
	for (int i = 0; i < 80; ++i)
	{
		// function
		uint32_t t{};
		if (i < 20)
			t = (d^(b&(c^d))) + k[0];
		else if (i < 40)
			t = (b^c^d) + k[1];
		else if (i < 60)
			t = ((b&c)|(d&(b|c))) + k[2];
		else if (i < 80)
			t = (b^c^d) + k[3];
		t += rl(a, 5)+e+w[i&0xF];
		b = rl(b,30);

		// rotate
		e = d; d = c; c = b; b = a; a = t;

		// extend
        w[i&0xF] = rl(w[i+13&0xF] ^ w[i+8&0xF] ^ w[i+2&0xF] ^ w[i&0xF], 1);
	}
	#else
	#define round(fn, a, b, c, d, e, w00, w02, w08, w13)\
		e += fn + rl(a,5) + w00;\
		b = rl(b,30);\
		w00 = rl(w13^w08^w02^w00, 1);
	#define roundA(a, b, c, d, e, w00, w02, w08, w13)\
		round((d^(b&(c^d)))+k[0], a, b, c, d, e, w00, w02, w08, w13)
	#define roundB(a, b, c, d, e, w00, w02, w08, w13)\
		round ((b^c^d)+k[1], a, b, c, d, e, w00, w02, w08, w13)
	#define roundC(a, b, c, d, e, w00, w02, w08, w13)\
		round (((b&c)|(d&(b|c)))+k[2], a, b, c, d, e, w00, w02, w08, w13)
	#define roundD(a, b, c, d, e, w00, w02, w08, w13)\
		round ((b^c^d)+k[3], a, b, c, d, e, w00, w02, w08, w13)

	roundA(a, b, c, d, e, w[ 0], w[ 2], w[ 8], w[13]); // round a00
	roundA(e, a, b, c, d, w[ 1], w[ 3], w[ 9], w[14]); // round a01
	roundA(d, e, a, b, c, w[ 2], w[ 4], w[10], w[15]); // round a02
	roundA(c, d, e, a, b, w[ 3], w[ 5], w[11], w[ 0]); // round a03
	roundA(b, c, d, e, a, w[ 4], w[ 6], w[12], w[ 1]); // round a04
	roundA(a, b, c, d, e, w[ 5], w[ 7], w[13], w[ 2]); // round a05
	roundA(e, a, b, c, d, w[ 6], w[ 8], w[14], w[ 3]); // round a06
	roundA(d, e, a, b, c, w[ 7], w[ 9], w[15], w[ 4]); // round a07
	roundA(c, d, e, a, b, w[ 8], w[10], w[ 0], w[ 5]); // round a08
	roundA(b, c, d, e, a, w[ 9], w[11], w[ 1], w[ 6]); // round a09
	roundA(a, b, c, d, e, w[10], w[12], w[ 2], w[ 7]); // round a10
	roundA(e, a, b, c, d, w[11], w[13], w[ 3], w[ 8]); // round a11
	roundA(d, e, a, b, c, w[12], w[14], w[ 4], w[ 9]); // round a12
	roundA(c, d, e, a, b, w[13], w[15], w[ 5], w[10]); // round a13
	roundA(b, c, d, e, a, w[14], w[ 0], w[ 6], w[11]); // round a14
	roundA(a, b, c, d, e, w[15], w[ 1], w[ 7], w[12]); // round a15
	roundA(e, a, b, c, d, w[ 0], w[ 2], w[ 8], w[13]); // round a16
	roundA(d, e, a, b, c, w[ 1], w[ 3], w[ 9], w[14]); // round a17
	roundA(c, d, e, a, b, w[ 2], w[ 4], w[10], w[15]); // round a18
	roundA(b, c, d, e, a, w[ 3], w[ 5], w[11], w[ 0]); // round a19
	roundB(a, b, c, d, e, w[ 4], w[ 6], w[12], w[ 1]); // round b00
	roundB(e, a, b, c, d, w[ 5], w[ 7], w[13], w[ 2]); // round b01
	roundB(d, e, a, b, c, w[ 6], w[ 8], w[14], w[ 3]); // round b02
	roundB(c, d, e, a, b, w[ 7], w[ 9], w[15], w[ 4]); // round b03
	roundB(b, c, d, e, a, w[ 8], w[10], w[ 0], w[ 5]); // round b04
	roundB(a, b, c, d, e, w[ 9], w[11], w[ 1], w[ 6]); // round b05
	roundB(e, a, b, c, d, w[10], w[12], w[ 2], w[ 7]); // round b06
	roundB(d, e, a, b, c, w[11], w[13], w[ 3], w[ 8]); // round b07
	roundB(c, d, e, a, b, w[12], w[14], w[ 4], w[ 9]); // round b08
	roundB(b, c, d, e, a, w[13], w[15], w[ 5], w[10]); // round b09
	roundB(a, b, c, d, e, w[14], w[ 0], w[ 6], w[11]); // round b10
	roundB(e, a, b, c, d, w[15], w[ 1], w[ 7], w[12]); // round b11
	roundB(d, e, a, b, c, w[ 0], w[ 2], w[ 8], w[13]); // round b12
	roundB(c, d, e, a, b, w[ 1], w[ 3], w[ 9], w[14]); // round b13
	roundB(b, c, d, e, a, w[ 2], w[ 4], w[10], w[15]); // round b14
	roundB(a, b, c, d, e, w[ 3], w[ 5], w[11], w[ 0]); // round b15
	roundB(e, a, b, c, d, w[ 4], w[ 6], w[12], w[ 1]); // round b16
	roundB(d, e, a, b, c, w[ 5], w[ 7], w[13], w[ 2]); // round b17
	roundB(c, d, e, a, b, w[ 6], w[ 8], w[14], w[ 3]); // round b18
	roundB(b, c, d, e, a, w[ 7], w[ 9], w[15], w[ 4]); // round b19
	roundC(a, b, c, d, e, w[ 8], w[10], w[ 0], w[ 5]); // round c00
	roundC(e, a, b, c, d, w[ 9], w[11], w[ 1], w[ 6]); // round c01
	roundC(d, e, a, b, c, w[10], w[12], w[ 2], w[ 7]); // round c02
	roundC(c, d, e, a, b, w[11], w[13], w[ 3], w[ 8]); // round c03
	roundC(b, c, d, e, a, w[12], w[14], w[ 4], w[ 9]); // round c04
	roundC(a, b, c, d, e, w[13], w[15], w[ 5], w[10]); // round c05
	roundC(e, a, b, c, d, w[14], w[ 0], w[ 6], w[11]); // round c06
	roundC(d, e, a, b, c, w[15], w[ 1], w[ 7], w[12]); // round c07
	roundC(c, d, e, a, b, w[ 0], w[ 2], w[ 8], w[13]); // round c08
	roundC(b, c, d, e, a, w[ 1], w[ 3], w[ 9], w[14]); // round c09
	roundC(a, b, c, d, e, w[ 2], w[ 4], w[10], w[15]); // round c10
	roundC(e, a, b, c, d, w[ 3], w[ 5], w[11], w[ 0]); // round c11
	roundC(d, e, a, b, c, w[ 4], w[ 6], w[12], w[ 1]); // round c12
	roundC(c, d, e, a, b, w[ 5], w[ 7], w[13], w[ 2]); // round c13
	roundC(b, c, d, e, a, w[ 6], w[ 8], w[14], w[ 3]); // round c14
	roundC(a, b, c, d, e, w[ 7], w[ 9], w[15], w[ 4]); // round c15
	roundC(e, a, b, c, d, w[ 8], w[10], w[ 0], w[ 5]); // round c16
	roundC(d, e, a, b, c, w[ 9], w[11], w[ 1], w[ 6]); // round c17
	roundC(c, d, e, a, b, w[10], w[12], w[ 2], w[ 7]); // round c18
	roundC(b, c, d, e, a, w[11], w[13], w[ 3], w[ 8]); // round c19
	roundD(a, b, c, d, e, w[12], w[14], w[ 4], w[ 9]); // round d00
	roundD(e, a, b, c, d, w[13], w[15], w[ 5], w[10]); // round d01
	roundD(d, e, a, b, c, w[14], w[ 0], w[ 6], w[11]); // round d02
	roundD(c, d, e, a, b, w[15], w[ 1], w[ 7], w[12]); // round d03
	roundD(b, c, d, e, a, w[ 0], w[ 2], w[ 8], w[13]); // round d04
	roundD(a, b, c, d, e, w[ 1], w[ 3], w[ 9], w[14]); // round d05
	roundD(e, a, b, c, d, w[ 2], w[ 4], w[10], w[15]); // round d06
	roundD(d, e, a, b, c, w[ 3], w[ 5], w[11], w[ 0]); // round d07
	roundD(c, d, e, a, b, w[ 4], w[ 6], w[12], w[ 1]); // round d08
	roundD(b, c, d, e, a, w[ 5], w[ 7], w[13], w[ 2]); // round d09
	roundD(a, b, c, d, e, w[ 6], w[ 8], w[14], w[ 3]); // round d10
	roundD(e, a, b, c, d, w[ 7], w[ 9], w[15], w[ 4]); // round d11
	roundD(d, e, a, b, c, w[ 8], w[10], w[ 0], w[ 5]); // round d12
	roundD(c, d, e, a, b, w[ 9], w[11], w[ 1], w[ 6]); // round d13
	roundD(b, c, d, e, a, w[10], w[12], w[ 2], w[ 7]); // round d14
	roundD(a, b, c, d, e, w[11], w[13], w[ 3], w[ 8]); // round d15
	roundD(e, a, b, c, d, w[12], w[14], w[ 4], w[ 9]); // round d16
	roundD(d, e, a, b, c, w[13], w[15], w[ 5], w[10]); // round d17
	roundD(c, d, e, a, b, w[14], w[ 0], w[ 6], w[11]); // round d18
	roundD(b, c, d, e, a, w[15], w[ 1], w[ 7], w[12]); // round d19
	#endif

	// add hash to digest
	hh[0] += a; hh[1] += b; hh[2] += c; hh[3] += d; hh[4] += e;
}
