// clmul implementations
// Copyright 2019 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#pragma once
// include files
#include <cstdint>

// clmul optimization on GCC for x86
#ifdef __x86_64__
#pragma GCC target("pclmul")
#include <immintrin.h>
inline uint64_t clmul32(uint32_t x, uint32_t y)
{
	return __builtin_ia32_pclmulqdq128(__v2di{x}, __v2di{y}, 0)[0];
}
#define CLMUL
#endif

// clmul optimization on VC for x86
#ifdef _MSC_VER
#ifdef _M_AMD64
#include <immintrin.h>
#pragma intrinsic(_mm_clmulepi64_si128)
inline uint64_t clmul32(uint32_t x, uint32_t y)
{
	__m128i x1, y1;
	x1.m128i_i64[0] = x; y1.m128i_i64[0] = y;
	return _mm_clmulepi64_si128(x1, y1, 0).m128i_i64[0];
}
#define CLMUL
#endif
#endif

// clmul optimization on GCC for ARM
// compile with gcc -mfpu=crypto-neon-fp-armv8 -D__ARM_FEATURE_CRYPT
#ifdef __ARM_FEATURE_CRYPT
#include <arm_neon.h>
inline uint64_t clmul32(uint32_t x, uint32_t y)
{
	return vmull_p64(x, y);
}
#define _CLMUL
#endif
