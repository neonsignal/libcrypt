// Copyright 2017 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

// AES128 encryption for use in CTR mode
#pragma once
#include <cstdint>
#include <stdint.h>
class Aes128
{
public:
	Aes128(const uint8_t *key);
	void encrypt(uint8_t *output);
private:
	static const int Nk{4}; // key words
	static const int Nr{10}; // rounds
	uint32_t roundkey[(Nr+1)*Nk];
};
