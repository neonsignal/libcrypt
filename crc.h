// Cyclic redundancy checks
// Copyright 1998-2024 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#pragma once

// include files
#include "clmul.h"
#include <cstdint>

inline uint32_t bswap32(uint32_t x)
{
	return __builtin_bswap32(x);
}

// polynomial inversion
template<typename Word, int bits = sizeof(Word)*8> constexpr uint32_t clinv32(uint32_t polynomial)
{
	uint64_t a = polynomial;
	for (int i = 0; i < 32; ++i)
	{
		a <<= 1;
		if (a & 1ull<<bits)
			a ^= polynomial;  // divisor
	}
	return a>>bits;
};

namespace math
{
// cyclic redundancy check using predefined table
// Word: unsigned type large enough to hold CRC
// crc_bits: number of bits in CRC (must be equal or less than the word size)
// polynomial: GF polynomial used for CRC (may need to be reversed)
// initial: initial value XOR
// final: final value XOR
// reverse: reverse bit CRC
template<typename Word, int crc_bits, Word polynomial, Word initial, Word final, bool reverse=false>
class Crc
{
public:
	// initialize CRC table
	// required if no crc_table specialization provided
	static void pre_initialize()
	{;
		// generate CRCs of all characters
		uint8_t c = 0;
		do
		{
			// xor with generator polynomial
			Word crc{c};
			if (!reverse)
			{
				crc <<= crc_bits-8;
				for (int i = 0; i < 8; i++)
					if (crc & (Word{1}<<crc_bits-1))
						crc = (crc << 1) ^ polynomial;
					else
						crc <<= 1;
			}
			else
			{
				for (int i = 0; i < 8; i++)
					if (crc & 0x1)
						crc = (crc >> 1) ^ polynomial;
					else
						crc >>= 1;
			}
			crc_table[c] = crc;
		}
		while (++c);
	}

	// create checksum
	Crc() : checksum{initial} {}

	// set checksum
	Crc &operator=(Word checksum)
	{
		checksum.value = checksum^final;
		return *this;
	}

	// get checksum;
	// return current checksum
	operator Word()
	{
		return checksum.value^final;
	}

	// supply another character to the checksum
	// c: character
	Crc &operator+=(const uint8_t c)
	{
		if (!reverse)
			checksum.value = crc_table[c ^ checksum.value>>crc_bits-8] ^ checksum.value<<8;
		else
			checksum.value = crc_table[c ^ checksum.value&0xFF] ^ checksum.value>>8;
		return *this;
	}

	// supply a string to the checksum
	// buffer: zero terminated string
	Crc &operator+=(const uint8_t *buffer)
	{
		// calculate checksum for each character
		while (*buffer)
			operator+=(*buffer++);
		return *this;
	}
private:
	struct {Word value:crc_bits;} checksum;
	static Word crc_table[256];
}; 

// cyclic redundancy check using clmul intrinsics
// Word: unsigned type large enough to hold CRC
// crc_bits: number of bits in CRC (must be equal or less than the word size)
// polynomial: GF polynomial used for CRC (may need to be reversed)
// initial: initial value XOR
// final: final value XOR
// reverse: reverse bit CRC (not implemented)
template<typename Word, int crc_bits, Word polynomial, Word initial, Word final, bool reverse=false>
class CrcX
{
	static constexpr uint32_t ipolynomial{clinv32<Word, crc_bits>(polynomial)};
public:
	// create checksum
	CrcX() : checksum{initial} {}

	// set checksum
	CrcX &operator=(Word checksum)
	{
		checksum.value = checksum^final;
		return *this;
	}

	// get checksum
	// return current checksum
	operator Word()
	{
		return checksum.value^final;
	}

	// supply another character to the checksum
	// c: character
	CrcX &operator+=(const uint8_t c)
	{
		uint32_t r = checksum.value>>crc_bits-8 ^ c;
		checksum.value = clmul32(r^clmul32(r, ipolynomial)>>32, polynomial) ^ checksum.value<<8;
		return *this;
	}

	// supply another 4 characters to the checksum.value
	// c: characters (dword)
	CrcX &operator+=(const uint32_t c)
	{
		// supply another character to the checksum.value
		uint32_t r = checksum.value<<32-crc_bits ^ bswap32(c);
		checksum.value = clmul32(r^clmul32(r, ipolynomial)>>32, polynomial);
		return *this;
	}

	// supply a string to the checksum
	// buffer: zero terminated string
	CrcX &operator+=(const uint8_t *buffer)
	{
		// calculate checksum for each character
		while (*buffer)
			operator+=(*buffer++);
		return *this;
	}
private:
	struct {Word value:crc_bits;} checksum;
};

// CRC templates with predefined tables in crc.cpp
using Crc16_xmodem = Crc<uint16_t, 16, 0x1021u, 0, 0>;
using Crc16_X25 = Crc<uint16_t, 16, 0xA001u, 0xFFFFu, 0xFFFFu, true>;
using Crc24_Q = Crc<uint32_t, 24, 0x864CFBul, 0x000000ul, 0x000000ul>;
using Crc32_posix = Crc<uint32_t, 32, 0x04C11DB7ul, 0, 0xFFFFFFFFul>;
using Crc64_ECMA182 = Crc<uint64_t, 64, 0x42F0E1EBA9EA3693ull, 0, 0>;
using Crc64_ISO = Crc<uint64_t, 64, 0xD800000000000000ull, 0xFFFFFFFFFFFFFFFFull, 0xFFFFFFFFFFFFFFFFull, true>;

// predefined CRC templates using clmul intrinsics
using CrcX16_xmodem = CrcX<uint16_t, 16, 0x1021u, 0, 0>;
using CrcX24_Q = CrcX<uint32_t, 24, 0x864CFBul, 0x000000ul, 0x000000ul>;
using CrcX32_posix = CrcX<uint32_t, 32, 0x04C11DB7ul, 0, 0xFFFFFFFFul>;
}
