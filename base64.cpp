// base64 encoding
// Copyright 2023 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
#include "base64.h"
using std::string;

// encoding
string Base64encode::operator()(const string plain)
{
	string code;
	uint32_t r{};
	int count{0};
	for (auto p = plain.begin(); p != plain.end();)
	{
		if (count < 6)
			r |= uint8_t(*p++)<<24-count;
		code += lookup[r>>26];
		r <<= 6;
		count = (count+2) % 8;
	}
	if (count)
	{
		code += lookup[r>>26];	
		count = (count+2) % 8;
		while (count)
		{
			code += '=';
			count = (count+2) % 8;
		}
	}
	return code;
}

// decoding
string Base64decode::operator()(const string code)
{
	string plain;
	uint32_t r{};
	int count{0};
	for (auto p = code.begin(); p != code.end() && *p != '=';)
	{
		r |= lookup[*p++ - 0x20]<<18+count;
		if (count > 0)
			plain += r>>24;
		r <<= 8;
		count = (count+2) % 8;
	}
	return plain;
}
