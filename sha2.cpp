// SHA256 hash
// Copyright 2017-2020 Glenn McIntosh
// licensed under the GNU General Public Licence version 3

// include files
#include "sha2.h"
#include <algorithm>

// register rotation
template<typename T> T rl(T x, const int i) {return x<<i | x>>(sizeof(T)*8-i);}
template<typename T, int n> constexpr int size(T(&)[n]){return n;}

void Sha2::begin()
{
	length = 0;
	hh[0] = 0x6a09e667u; hh[1] = 0xbb67ae85u; hh[2] = 0x3c6ef372u; hh[3] = 0xa54ff53au; hh[4] = 0x510e527fu; hh[5] = 0x9b05688cu; hh[6] = 0x1f83d9abu; hh[7] = 0x5be0cd19u;
	w_current = w_begin;
}

void Sha2::add(const uint8_t *data, int n)
{
	// hash message blocks
	const uint8_t *datan = data+n, *data1;
	while ((data1 = data+(w_end-w_current)) <= datan)
	{
		std::copy(data, data1, w_current);
		data = data1;
		w_current = w_begin;
		block();
	}

	// save remainder
	std::copy(data, datan, w_current);
	w_current += datan-data;
	length += n;
}

void Sha2::end()
{
	// terminate and finalize hash
	*w_current++ = 0x80;
	if (w_current > w_end-sizeof(uint64_t))
	{
		std::fill(w_current, w_end, 0x00);
		block();
		w_current = w_begin;
	}
	std::fill(w_current, w_end-sizeof(uint64_t), 0x00);
	w[::size(w)-2] = __builtin_bswap32(length*8 >> 32);
	w[::size(w)-1] = __builtin_bswap32(length*8);
	block();

	// back to little endian
	for (int i = 0; i < ::size(hh); ++i)
		hh[i] = __builtin_bswap32(hh[i]);
}

// hash
void Sha2::block()
{
	// change to big endian
	for (int j = 0; j < ::size(w); ++j)
		w[j] = __builtin_bswap32(w[j]);

	// initialize hash
	uint32_t a = hh[0], b = hh[1], c = hh[2], d = hh[3], e = hh[4], f = hh[5], g = hh[6], h = hh[7];

	// round keys
	const uint32_t k[] =
	{
		0x428a2f98u, 0x71374491u, 0xb5c0fbcfu, 0xe9b5dba5u, 0x3956c25bu, 0x59f111f1u, 0x923f82a4u, 0xab1c5ed5u,
		0xd807aa98u, 0x12835b01u, 0x243185beu, 0x550c7dc3u, 0x72be5d74u, 0x80deb1feu, 0x9bdc06a7u, 0xc19bf174u,
		0xe49b69c1u, 0xefbe4786u, 0x0fc19dc6u, 0x240ca1ccu, 0x2de92c6fu, 0x4a7484aau, 0x5cb0a9dcu, 0x76f988dau,
		0x983e5152u, 0xa831c66du, 0xb00327c8u, 0xbf597fc7u, 0xc6e00bf3u, 0xd5a79147u, 0x06ca6351u, 0x14292967u,
		0x27b70a85u, 0x2e1b2138u, 0x4d2c6dfcu, 0x53380d13u, 0x650a7354u, 0x766a0abbu, 0x81c2c92eu, 0x92722c85u,
		0xa2bfe8a1u, 0xa81a664bu, 0xc24b8b70u, 0xc76c51a3u, 0xd192e819u, 0xd6990624u, 0xf40e3585u, 0x106aa070u,
		0x19a4c116u, 0x1e376c08u, 0x2748774cu, 0x34b0bcb5u, 0x391c0cb3u, 0x4ed8aa4au, 0x5b9cca4fu, 0x682e6ff3u,
		0x748f82eeu, 0x78a5636fu, 0x84c87814u, 0x8cc70208u, 0x90befffau, 0xa4506cebu, 0xbef9a3f7u, 0xc67178f2u,
	};

	// rounds
	#if !defined(__OPTIMIZE__) || defined(__OPTIMEYE_SIZE__)
	for (int i = 0; i < 64; ++i)
	{
		// function
		uint32_t t = w[i&0xF] + k[i] + h;
		t += (e&f) ^ (~e&g);
		t += rl(e, 26) ^ rl(e, 21) ^ rl(e, 7);
		d += t;
		t += (a&b) ^ (a&c) ^ (b&c);
		t += rl(a, 30) ^ rl(a, 19) ^ rl(a, 10);

		// shift
		h = g; g = f; f = e; e = d; d = c; c = b; b = a; a = t;

		// extend
		w[i+16&0xF] = w[i+0&0xF] + (rl(w[i+1&0xF], 25) ^ rl(w[i+1&0xF], 14) ^ w[i+1&0xF]>>3) + w[i+9&0xF] + (rl(w[i+14&0xF], 15) ^ rl(w[i+14&0xF], 13) ^ w[i+14&0xF]>>10);
	}
	#else
	#define round(k, a, b, c, d, e, f, g, h, w00, w01, w09, w14)\
		h += w00 + k;\
		h += (e&f) ^ (~e&g);\
		h += rl(e, 26) ^ rl(e, 21) ^ rl(e, 7);\
		d += h;\
		h += (a&b) ^ (a&c) ^ (b&c);\
		h += rl(a, 30) ^ rl(a, 19) ^ rl(a, 10);\
		w00 += (rl(w01, 25) ^ rl(w01, 14) ^ w01>>3) + w09 + (rl(w14, 15) ^ rl(w14, 13) ^ w14>>10);
	round(k[ 0], a, b, c, d, e, f, g, h, w[ 0], w[ 1], w[ 9], w[14]); // round a00
	round(k[ 1], h, a, b, c, d, e, f, g, w[ 1], w[ 2], w[10], w[15]); // round a01
	round(k[ 2], g, h, a, b, c, d, e, f, w[ 2], w[ 3], w[11], w[ 0]); // round a02
	round(k[ 3], f, g, h, a, b, c, d, e, w[ 3], w[ 4], w[12], w[ 1]); // round a03
	round(k[ 4], e, f, g, h, a, b, c, d, w[ 4], w[ 5], w[13], w[ 2]); // round a04
	round(k[ 5], d, e, f, g, h, a, b, c, w[ 5], w[ 6], w[14], w[ 3]); // round a05
	round(k[ 6], c, d, e, f, g, h, a, b, w[ 6], w[ 7], w[15], w[ 4]); // round a06
	round(k[ 7], b, c, d, e, f, g, h, a, w[ 7], w[ 8], w[ 0], w[ 5]); // round a07
	round(k[ 8], a, b, c, d, e, f, g, h, w[ 8], w[ 9], w[ 1], w[ 6]); // round a08
	round(k[ 9], h, a, b, c, d, e, f, g, w[ 9], w[10], w[ 2], w[ 7]); // round a09
	round(k[10], g, h, a, b, c, d, e, f, w[10], w[11], w[ 3], w[ 8]); // round a10
	round(k[11], f, g, h, a, b, c, d, e, w[11], w[12], w[ 4], w[ 9]); // round a11
	round(k[12], e, f, g, h, a, b, c, d, w[12], w[13], w[ 5], w[10]); // round a12
	round(k[13], d, e, f, g, h, a, b, c, w[13], w[14], w[ 6], w[11]); // round a13
	round(k[14], c, d, e, f, g, h, a, b, w[14], w[15], w[ 7], w[12]); // round a14
	round(k[15], b, c, d, e, f, g, h, a, w[15], w[ 0], w[ 8], w[13]); // round a15
	round(k[16], a, b, c, d, e, f, g, h, w[ 0], w[ 1], w[ 9], w[14]); // round b00
	round(k[17], h, a, b, c, d, e, f, g, w[ 1], w[ 2], w[10], w[15]); // round b01
	round(k[18], g, h, a, b, c, d, e, f, w[ 2], w[ 3], w[11], w[ 0]); // round b02
	round(k[19], f, g, h, a, b, c, d, e, w[ 3], w[ 4], w[12], w[ 1]); // round b03
	round(k[20], e, f, g, h, a, b, c, d, w[ 4], w[ 5], w[13], w[ 2]); // round b04
	round(k[21], d, e, f, g, h, a, b, c, w[ 5], w[ 6], w[14], w[ 3]); // round b05
	round(k[22], c, d, e, f, g, h, a, b, w[ 6], w[ 7], w[15], w[ 4]); // round b06
	round(k[23], b, c, d, e, f, g, h, a, w[ 7], w[ 8], w[ 0], w[ 5]); // round b07
	round(k[24], a, b, c, d, e, f, g, h, w[ 8], w[ 9], w[ 1], w[ 6]); // round b08
	round(k[25], h, a, b, c, d, e, f, g, w[ 9], w[10], w[ 2], w[ 7]); // round b09
	round(k[26], g, h, a, b, c, d, e, f, w[10], w[11], w[ 3], w[ 8]); // round b10
	round(k[27], f, g, h, a, b, c, d, e, w[11], w[12], w[ 4], w[ 9]); // round b11
	round(k[28], e, f, g, h, a, b, c, d, w[12], w[13], w[ 5], w[10]); // round b12
	round(k[29], d, e, f, g, h, a, b, c, w[13], w[14], w[ 6], w[11]); // round b13
	round(k[30], c, d, e, f, g, h, a, b, w[14], w[15], w[ 7], w[12]); // round b14
	round(k[31], b, c, d, e, f, g, h, a, w[15], w[ 0], w[ 8], w[13]); // round b15
	round(k[32], a, b, c, d, e, f, g, h, w[ 0], w[ 1], w[ 9], w[14]); // round c00
	round(k[33], h, a, b, c, d, e, f, g, w[ 1], w[ 2], w[10], w[15]); // round c01
	round(k[34], g, h, a, b, c, d, e, f, w[ 2], w[ 3], w[11], w[ 0]); // round c02
	round(k[35], f, g, h, a, b, c, d, e, w[ 3], w[ 4], w[12], w[ 1]); // round c03
	round(k[36], e, f, g, h, a, b, c, d, w[ 4], w[ 5], w[13], w[ 2]); // round c04
	round(k[37], d, e, f, g, h, a, b, c, w[ 5], w[ 6], w[14], w[ 3]); // round c05
	round(k[38], c, d, e, f, g, h, a, b, w[ 6], w[ 7], w[15], w[ 4]); // round c06
	round(k[39], b, c, d, e, f, g, h, a, w[ 7], w[ 8], w[ 0], w[ 5]); // round c07
	round(k[40], a, b, c, d, e, f, g, h, w[ 8], w[ 9], w[ 1], w[ 6]); // round c08
	round(k[41], h, a, b, c, d, e, f, g, w[ 9], w[10], w[ 2], w[ 7]); // round c09
	round(k[42], g, h, a, b, c, d, e, f, w[10], w[11], w[ 3], w[ 8]); // round c10
	round(k[43], f, g, h, a, b, c, d, e, w[11], w[12], w[ 4], w[ 9]); // round c11
	round(k[44], e, f, g, h, a, b, c, d, w[12], w[13], w[ 5], w[10]); // round c12
	round(k[45], d, e, f, g, h, a, b, c, w[13], w[14], w[ 6], w[11]); // round c13
	round(k[46], c, d, e, f, g, h, a, b, w[14], w[15], w[ 7], w[12]); // round c14
	round(k[47], b, c, d, e, f, g, h, a, w[15], w[ 0], w[ 8], w[13]); // round c15
	round(k[48], a, b, c, d, e, f, g, h, w[ 0], w[ 1], w[ 9], w[14]); // round d00
	round(k[49], h, a, b, c, d, e, f, g, w[ 1], w[ 2], w[10], w[15]); // round d01
	round(k[50], g, h, a, b, c, d, e, f, w[ 2], w[ 3], w[11], w[ 0]); // round d02
	round(k[51], f, g, h, a, b, c, d, e, w[ 3], w[ 4], w[12], w[ 1]); // round d03
	round(k[52], e, f, g, h, a, b, c, d, w[ 4], w[ 5], w[13], w[ 2]); // round d04
	round(k[53], d, e, f, g, h, a, b, c, w[ 5], w[ 6], w[14], w[ 3]); // round d05
	round(k[54], c, d, e, f, g, h, a, b, w[ 6], w[ 7], w[15], w[ 4]); // round d06
	round(k[55], b, c, d, e, f, g, h, a, w[ 7], w[ 8], w[ 0], w[ 5]); // round d07
	round(k[56], a, b, c, d, e, f, g, h, w[ 8], w[ 9], w[ 1], w[ 6]); // round d08
	round(k[57], h, a, b, c, d, e, f, g, w[ 9], w[10], w[ 2], w[ 7]); // round d09
	round(k[58], g, h, a, b, c, d, e, f, w[10], w[11], w[ 3], w[ 8]); // round d10
	round(k[59], f, g, h, a, b, c, d, e, w[11], w[12], w[ 4], w[ 9]); // round d11
	round(k[60], e, f, g, h, a, b, c, d, w[12], w[13], w[ 5], w[10]); // round d12
	round(k[61], d, e, f, g, h, a, b, c, w[13], w[14], w[ 6], w[11]); // round d13
	round(k[62], c, d, e, f, g, h, a, b, w[14], w[15], w[ 7], w[12]); // round d14
	round(k[63], b, c, d, e, f, g, h, a, w[15], w[ 0], w[ 8], w[13]); // round d15
	#endif

	// add hash to digest
	hh[0] += a; hh[1] += b; hh[2] += c; hh[3] += d; hh[4] += e; hh[5] += f; hh[6] += g; hh[7] += h;
}
